{
  description = "Flake to install/run the livingice webpage/functionality";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };
  outputs = { self, nixpkgs }:

    let 

      forAllSystems = drv:
        nixpkgs.lib.genAttrs [
          "aarch64-darwin"
          "aarch64-linux"
          "x86_64-linux"
          "x86_64-darwin"
          "x86_64-windows"
        ] (system: drv nixpkgs.legacyPackages.${system});

    in {

        packages = forAllSystems (pkgs: {
          default = import ./default.nix {inherit pkgs;};
        });
        apps = forAllSystems (pkgs: 
          builtins.listToAttrs (builtins.map (name: (
                    {
                      inherit name;
                      value = {
                        type= "app";
                        program = "${self.packages.${pkgs.system}.default}/bin/livingice-${name}";
                      };
                    }
                  )) ["web" "spheres" "rephotos"])
           
        );

        overlays.default = final: prev: {
          livingice = self.packages.${prev.system}.default;
        };
      };
}