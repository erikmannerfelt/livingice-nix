This is a barebones instruction set using nix to fetch and run the [livingiceweb](https://gitlab.com/erikmannerfelt/livingiceweb) website.

## Running

The first time (or if the static content has been updated), the files have to be downloaded:
```nix
nix run gitlab:erikmannerfelt/livingice-nix#spheres
nix run gitlab:erikmannerfelt/livingice-nix#rephotos
```

Then, the webpage can be started:
```nix
nix run gitlab:erikmannerfelt/livingice-nix#web 
```
