{pkgs ? import <nixpkgs> {}, port ? "5010", static_url ? "https://static.livingiceproject.com/"}:

let
  src = pkgs.fetchFromGitLab {
    owner = "erikmannerfelt";
    repo = "livingiceweb";
    rev = "e2fc6d45d24e3aa7c5c861826a46cc17cea1342c";
    sha256="l6t390HJm+1dssY/EbmJwpO7nEZKpxZXcnMCdsLK9Aw=";
  };

  livingice = import (src.out + "/nix/default.nix") {inherit pkgs;};

in pkgs.stdenv.mkDerivation rec {

  pname = "livingice-bin";
  name = pname;

  buildInputs = [livingice];
  nativeBuildInputs = [pkgs.makeWrapper];
  unpackPhase = ''true'';

  buildPhase = ''
    mkdir -p $out/bin/
    ls ${livingice}/bin/ | while read binary; do
      makeWrapper "${livingice}/bin/$binary" "$out/bin/livingice-$binary"\
       --set ROCKET_PORT ${port}\
       --set ROCKET_ADDRESS 0.0.0.0\
       --set SERVER_URL ${static_url} 
    done

  '';
}
